/*
02
USER: Robin Halder Dev
03
TASK:
04
ALGO:
05
*/

/*
#pragma warning (disable: 4786)
#pragma comment (linker, "/STACK:0x800000")

*/

#include <cassert>
#include <cctype>
#include <cmath>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <stack>
#include <queue>
#include <algorithm>
#include <iterator>
#include <utility>
using namespace std;

template< class T > T _abs(T n) { return (n < 0 ? -n : n); }
template< class T > T _max(T a, T b) { return (!(a < b) ? a : b); }
template< class T > T _min(T a, T b) { return (a < b ? a : b); }
template< class T > T sq(T x) { return x * x; }
template< class T > T gcd(T a, T b) { return (b != 0 ? gcd<T>(b, a%b) : a); }
template< class T > T lcm(T a, T b) { return (a / gcd<T>(a, b) * b); }
template< class T > bool inside(T a, T b, T c) { return a<=b && b<=c; }

#define MP(x, y) make_pair(x, y)
#define SET(p) memset(p, -1, sizeof(p))
#define CLR(p) memset(p, 0, sizeof(p))
#define MEM(p, v) memset(p, v, sizeof(p))
#define CPY(d, s) memcpy(d, s, sizeof(s))
#define READ(f) freopen(f, "r", stdin)
#define WRITE(f) freopen(f, "w", stdout)
#define SZ(c) (int)c.size()
#define PB(x) push_back(x)
#define ff first
#define ss second
#define LL long long
#define LD long double
#define pii pair< int, int >
#define psi pair< string, int >
#define CASE_print printf("Case %d: ",++cases);

const double EPS = 1e-9;
const int INF = 999999;
const double PI = acos(-1.0);

vector<char>v[500];


void Fibo(){

  int i,j,k,carry=0;

        v[0].push_back(49);
        v[1].push_back(49);

          for(i=2;i<=500;i++){
            for(j=0;j<v[i-1].size();j++){

                 int aa= v[i-1][j]-48 ;
                 int bb=(j<v[i-2].size()) ? v[i-2][j]-48 : 0;
                 carry+=aa+bb;

                 v[i].PB(carry%10+48);
                 carry/=10;
            }

            if(carry){
               v[i].PB(carry+48);
               carry=0;
            }
        }


}

int cmp_big(string a,string b){

       if(a.size() > b.size())
         return 0;
       if(b.size() > a.size())
         return 1;

        for(int i=0;i<a.size();i++)
          if(b[i] > a[i])
            return 1;
          else if(b[i]!=a[i])
            return 0;

        return 1;

}

int cmp_small(string a,string b){


      if(a.size() > b.size())
         return 1;
       if(b.size() > a.size())
         return 0;

        for(int i=0;i<a.size();i++)
          if(b[i] < a[i])
            return 1;
          else if(b[i]!=a[i])
            return 0;

        return 1;


}

int main() {

    int i,j,k,cases=0,n,t;

        string a,b;

        Fibo();


          while(cin>>a>>b){

                if(a=="0" && b=="0")
                 break;
               int result=0;

                 for(i=1;i<500;i++){

                    string s="";
                     for(k=v[i].size()-1;k>=0;k--)
                       s+=v[i][k];

                     if(cmp_big(a,s))
                       break;
                 }

                 for(i;i<500;i++){

                    string ss="";
                    for(k=v[i].size()-1;k>=0;k--)
                       ss+=v[i][k];

                    if(cmp_small(b,ss))
                     result++;
                    else
                     break;
                 }



                printf("%d\n",result);

          }

    return 0;

}
